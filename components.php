<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Concept Construction</title>
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
</head>

<body>
    <main>
        <header class="main-header">
            <div class="logo">
                <a href="index.php"><img src="assets/img/logo-black.svg" alt="" /></a>
            </div>
            <div class="navLink-container">
                <input type="checkbox" class="menu-btn" id="menu-btn" />
                <label for="menu-btn" class="menu-icon">
                    <span class="menu-icon__line"></span>
                </label>

                <ul class="nav-links">
                    <li class="nav-link">
                        <a href="/">
                            Home
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="">About Us</a>
                    </li>
                    <li class="nav-link">
                        <a href="">Services</a>
                    </li>
                    <li class="nav-link">
                        <a href="javascript:void(0)">
                            Blog
                        </a>
                    </li>
                    <li class="nav-link DesktopOnly">
                        <a href="" class="primaryCta">
                            <span class="btnText"> Contact us</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)"></path>
                                </svg>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </header>
        <section class="Section extraPadded hp-heroSection compoBannerSec">
            <div class="container">
                <div class="heroWrapper">
                    <div class="heroContent">
                        <h6 class="tagline-text">
                            <span class="small-line"></span>construction
                        </h6>
                        <h1>JCB HEAVY CONSTRUCTION EQUIPMENT</h1>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
                            quam, vulputate in laoreet quis, tempus non diam.
                        </p>
                        <a href="" class="primaryCta">
                            <span class="btnText">Contact Us</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)" />
                                </svg>
                            </span>
                        </a>
                    </div>
                    <div class="heroImage">
                        <span class="circleDec"></span>
                        <img src="assets/img/jcb-home.png" alt="" />
                    </div>
                </div>
            </div>
        </section>

        <section class="Section ModernProperty">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-12">
                        <div class="BannerContent">
                            <h4 class="LeftWhiteElement RightWhiteElement">CONSTRUCTION
                            </h4>
                            <h1>Jcb Heavy Construction Equipment</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur <br>adipiscing elit.</p>
                            <a href="" class="primaryCta">
                                <span class="btnText">Contact Us</span>
                                <span class="btnArrow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                        viewBox="0 0 27.552 12.665">
                                        <path
                                            d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                            transform="translate(9.841 -14.624)"></path>
                                    </svg>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="Section extraPadded hp-heroSection compoBannerSec2">
            <div class="container">
                <div class="heroWrapper">
                    <div class="heroContent">
                        <h6 class="tagline-text">
                            <span class="small-line"></span>construction
                        </h6>
                        <h1>JCB HEAVY CONSTRUCTION EQUIPMENT</h1>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
                            quam, vulputate in laoreet quis, tempus non diam.
                        </p>
                        <a href="" class="primaryCta">
                            <span class="btnText">Contact Us</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)" />
                                </svg>
                            </span>
                        </a>
                    </div>
                    <div class="heroImage">
                        <span class="circleDec"></span>
                        <img src="assets/img/components/construction-2.png" alt="" />
                    </div>
                </div>
            </div>
        </section>



        <section class="InsideBanner secmt">
            <picture>
                <source media="(min-width:650px)" srcset="assets/img/components/about-bg.svg">
                <source media="(min-width:465px)" srcset="assets/img/components/about-bg.svg">
                <img src="assets/img/components/about-bg.svg" alt="Z-sites" style="width:auto;">
            </picture>
            <div class="BannerContent">
                <div class="container">
                    <div class="content">
                        <h1>About us</h1>
                        <ul>
                            <li>Home</li>
                            <li><a href="#">About us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section extraPadded rightImageLeftContentSection">
            <div class="container">
                <div class="leftRightContainer" style="--gap-between: 8rem">
                    <div class="left">
                        <img src="assets/img/jcb-home 2.png" alt="" />
                    </div>
                    <div class="right">
                        <h6 class="tagline-text">
                            <span class="small-line"></span> Right content left image
                        </h6>
                        <h2>I try and maintain a little bit of fitness, man.</h2>
                        <p class="subcopy">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
                            quam, vulputate in laoreet quis, tempus non diam. Donec placerat quam
                            vel purus semper ultricies. Pellentesque dictum sed libero non
                            feugiat. Ut in lacus sit amet risus convallis sollicitudin. Nam
                            pulvinar elementum commodo.
                        </p>
                        <p class="subcopy">
                            Ut tortor sapien, tempus in tincidunt sit amet, pulvinar non justo. Ut
                            in lacus sit amet risus convallis sollicitudin.
                        </p>

                        <a href="javascript:;" class="primaryCta mt50">
                            <span class="btnText">Know More</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)" />
                                </svg>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="leftImageRightContentSection">
            <div class="container">
                <div class="leftRightContainer reverse" style="--gap-between: 8rem">
                    <div class="left">
                        <img src="assets/img/jcb-home.png" alt="" />
                    </div>
                    <div class="right">
                        <h6 class="tagline-text">
                            <span class="small-line"></span> Right content left image
                        </h6>
                        <h2>I try and maintain a little bit of fitness, man.</h2>
                        <p class="subcopy">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
                            quam, vulputate in laoreet quis, tempus non diam. Donec placerat quam
                            vel purus semper ultricies. Pellentesque dictum sed libero non
                            feugiat. Ut in lacus sit amet risus convallis sollicitudin. Nam
                            pulvinar elementum commodo.
                        </p>
                        <p class="subcopy">
                            Ut tortor sapien, tempus in tincidunt sit amet, pulvinar non justo. Ut
                            in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum
                            commodo. Ut in lacus sit amet risus convallis sollicitudin. Nam
                            pulvinar elementum commodo.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section extraPadded rightImageLeftContentSection">
            <div class="container">
                <div class="leftRightContainer" style="--gap-between: 8rem">
                    <div class="left">
                        <img src="assets/img/jcb-home 2.png" alt="" />
                    </div>
                    <div class="right">
                        <h6 class="tagline-text">
                            <span class="small-line"></span> Right content left image
                        </h6>
                        <h2>I try and maintain a little bit of fitness, man.</h2>
                        <p class="subcopy">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
                            quam, vulputate in laoreet quis, tempus non diam. Donec placerat quam
                            vel purus semper ultricies. Pellentesque dictum sed libero non
                            feugiat. Ut in lacus sit amet risus convallis sollicitudin. Nam
                            pulvinar elementum commodo.
                        </p>
                        <p class="subcopy">
                            Ut tortor sapien, tempus in tincidunt sit amet, pulvinar non justo. Ut
                            in lacus sit amet risus convallis sollicitudin.
                        </p>

                        <a href="javascript:;" class="primaryCta mt50">
                            <span class="btnText">Know More</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)" />
                                </svg>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="leftImageRightContentSection withoutImageDesing">
            <div class="container">
                <div class="leftRightContainer reverse" style="--gap-between: 8rem">
                    <div class="left">
                        <img src="assets/img/components/constn-1.png" alt="" />
                    </div>
                    <div class="right">
                        <h6 class="tagline-text">
                            <span class="small-line"></span> Right content left image
                        </h6>
                        <h2>I try and maintain a little bit of fitness, man.</h2>
                        <p class="subcopy">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
                            quam, vulputate in laoreet quis, tempus non diam. Donec placerat quam
                            vel purus semper ultricies. Pellentesque dictum sed libero non
                            feugiat. Ut in lacus sit amet risus convallis sollicitudin. Nam
                            pulvinar elementum commodo.
                        </p>
                        <p class="subcopy">
                            Ut tortor sapien, tempus in tincidunt sit amet, pulvinar non justo. Ut
                            in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum
                            commodo. Ut in lacus sit amet risus convallis sollicitudin. Nam
                            pulvinar elementum commodo.
                        </p>

                        <a href="javascript:;" class="primaryCta mt50">
                            <span class="btnText">Know More</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)" />
                                </svg>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section extraPadded rightImageLeftContentSection withoutImageDesing">
            <div class="container">
                <div class="leftRightContainer" style="--gap-between: 8rem">
                    <div class="left">
                        <img src="assets/img/components/constn-2.png" alt="" />
                    </div>
                    <div class="right">
                        <h6 class="tagline-text">
                            <span class="small-line"></span> Right content left image
                        </h6>
                        <h2>I try and maintain a little bit of fitness, man.</h2>
                        <p class="subcopy">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
                            quam, vulputate in laoreet quis, tempus non diam. Donec placerat quam
                            vel purus semper ultricies. Pellentesque dictum sed libero non
                            feugiat. Ut in lacus sit amet risus convallis sollicitudin. Nam
                            pulvinar elementum commodo.
                        </p>
                        <p class="subcopy">
                            Ut tortor sapien, tempus in tincidunt sit amet, pulvinar non justo. Ut
                            in lacus sit amet risus convallis sollicitudin.
                        </p>

                        <a href="javascript:;" class="primaryCta mt50">
                            <span class="btnText">Know More</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)" />
                                </svg>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="leftImageRightContentSection withoutImageDesing">
            <div class="container">
                <div class="leftRightContainer reverse" style="--gap-between: 8rem">
                    <div class="left">
                        <img src="assets/img/components/constn-3.png" alt="" />
                    </div>
                    <div class="right">
                        <h6 class="tagline-text">
                            <span class="small-line"></span> Right content left image
                        </h6>
                        <h2>I try and maintain a little bit of fitness, man.</h2>
                        <p class="subcopy">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
                            quam, vulputate in laoreet quis, tempus non diam. Donec placerat quam
                            vel purus semper ultricies. Pellentesque dictum sed libero non
                            feugiat. Ut in lacus sit amet risus convallis sollicitudin. Nam
                            pulvinar elementum commodo.
                        </p>
                        <p class="subcopy">
                            Ut tortor sapien, tempus in tincidunt sit amet, pulvinar non justo. Ut
                            in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum
                            commodo. Ut in lacus sit amet risus convallis sollicitudin. Nam
                            pulvinar elementum commodo.
                        </p>

                        <a href="javascript:;" class="primaryCta mt50">
                            <span class="btnText">Know More</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)" />
                                </svg>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section extraPadded rightImageLeftContentSection withoutImageDesing">
            <div class="container">
                <div class="leftRightContainer" style="--gap-between: 8rem">
                    <div class="left">
                        <h2>I try and maintain a little bit of fitness, man.</h2>
                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
                            quam, vulputate in laoreet quis, tempus non diam.
                            Pellentesque dictum sed libero non</h5>
                        <p class="subcopy">
                            Ut tortor sapien, tempus in tincidunt sit amet, pulvinar non justo. Ut
                            in lacus sit amet risus convallis sollicitudin.
                        </p>

                        <a href="javascript:;" class="primaryCta mt50">
                            <span class="btnText">Know More</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)" />
                                </svg>
                            </span>
                        </a>
                    </div>
                    <div class="right">
                        <h2>I try and maintain a little bit of fitness, man.</h2>
                        <p class="subcopy">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
                            quam, vulputate in laoreet quis, tempus non diam. Donec placerat quam
                            vel purus semper ultricies. Pellentesque dictum sed libero non
                            feugiat. Ut in lacus sit amet risus convallis sollicitudin. Nam
                            pulvinar elementum commodo.
                        </p>
                        <p class="subcopy">
                            Ut tortor sapien, tempus in tincidunt sit amet, pulvinar non justo. Ut
                            in lacus sit amet risus convallis sollicitudin.
                        </p>

                        <a href="javascript:;" class="primaryCta mt50">
                            <span class="btnText">Know More</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)" />
                                </svg>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="TypographySection">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="RightContent">
                            <h1 class="hOneTypo mt-0">Typography</h1>
                            <h1 class="hOneTypo">H1 A Visual Type Scale</h1>
                            <h2 class="hTwoTypo">H2 A Visual Type Scale</h2>
                            <h3 class="hThreeTypo">H3 A Visual Type Scale</h3>
                            <h4 class="hFourTypo">H4 A Visual Type Scale</h4>
                        </div>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="RightContent d-f">
                            <div>
                                <h2 class="hTwoTypo">Primary button</h2>
                                <a href="javascript:;" class="primaryCta">
                                    <span class="btnText">Read More</span>
                                    <span class="btnArrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                            viewBox="0 0 27.552 12.665">
                                            <path
                                                d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                                transform="translate(9.841 -14.624)" />
                                        </svg>
                                    </span>
                                </a>
                            </div>
                            <div>
                                <h2 class="hTwoTypo">Secondary button</h2>
                                <a href="javascript:;" class="primaryCta secondaryCta">
                                    <span class="btnText">Read More</span>
                                    <span class="btnArrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                            viewBox="0 0 27.552 12.665">
                                            <path
                                                d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                                transform="translate(9.841 -14.624)" />
                                        </svg>
                                    </span>
                                </a>
                            </div>
                            <button aria-label="Previous" type="button" style=""><img src="assets/img/arrow-left.svg"
                                    alt=""></button>
                            <button aria-label="Next" type="button" style=""><img src="assets/img/arrow.svg"
                                    alt=""></button>
                        </div>
                        <div class="RightContent df mt-50">
                            <a href="#" class="primaryLinkButton ReadMoreYellow d-f">
                                <span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
                            </a>
                            <a href="#" class="primaryLinkButton ReadMoreYellow d-f ReadMoreBlue">
                                <span>Read More <img src="assets/img/blue-arw.svg" alt=""></span>
                            </a>

                        </div>
                        <div class="RightContent mt-50">
                            <h2 class="hTwoTypo">Text Field</h2>
                            <div class="d-f"><input type="text" placeholder="Full Name">
                                <select name="" id="">
                                    <option value="">Subject</option>
                                    <option value="">Subject</option>
                                    <option value="">Subject</option>
                                    <option value="">Subject</option>
                                </select></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="Section extraPadded servicesCard-grid FourCardsSec">
            <div class="container">
                <div class="fourColWithCenteredOrphans">
                    <div class="servicesCard colItems categoryCards">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-1.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Warm up</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <a href="" class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="servicesCard colItems categoryCards">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-2.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Spin cycling</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <a href="" class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="servicesCard colItems categoryCards">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-3.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Warm up</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <a href="" class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="servicesCard colItems categoryCards">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-1.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Warm up</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <a href="" class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="servicesCard-grid FourCardsSec">
            <div class="container">
                <div class="fourColWithCenteredOrphans">
                    <div class="servicesCard colItems categoryCards">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-1.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Warm up</h5>
                        </div>
                    </div>
                    <div class="servicesCard colItems categoryCards">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-2.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Spin cycling</h5>
                        </div>
                    </div>
                    <div class="servicesCard colItems categoryCards">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-3.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Warm up</h5>
                        </div>
                    </div>
                    <div class="servicesCard colItems categoryCards">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-1.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Warm up</h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section ourCategory FourCardsOurCategory">
            <div class="container">
                <div class="fourColWithCenteredOrphans">
                    <div class="colItems categoryCards">
                        <div class="categoryCards-icon">
                            <?php @include('template-parts/svg-components/safety.php') ?>
                        </div>
                        <div class="categoryCards-content">
                            <h5>Safety</h5>
                            <p>There are many new variations of pasages of available text.</p>
                            <a href="" class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="colItems categoryCards">
                        <div class="categoryCards-icon">
                            <?php @include('template-parts/svg-components/community.php') ?>
                        </div>
                        <div class="categoryCards-content">
                            <h5>Community</h5>
                            <p>There are many new variations of pasages of available text.</p><a href=""
                                class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="colItems categoryCards">
                        <div class="categoryCards-icon">
                            <?php @include('template-parts/svg-components/sustain.php') ?>
                        </div>
                        <div class="categoryCards-content">
                            <h5>Sustanability</h5>
                            <p>There are many new variations of pasages of available text.</p><a href=""
                                class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="colItems categoryCards">
                        <div class="categoryCards-icon">
                            <?php @include('template-parts/svg-components/integrity.php') ?>
                        </div>
                        <div class="categoryCards-content">
                            <h5>Integrity</h5>
                            <p>There are many new variations of pasages of available text.</p><a href=""
                                class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="ourCategory ourCategoryWithoutIcon FourCardsOurCategory">
            <div class="container">
                <div class="fourColWithCenteredOrphans">
                    <div class="colItems categoryCards">
                        <div class="categoryCards-content">
                            <h5>Safety</h5>
                            <p>There are many new variations of pasages of available text.</p>
                            <a href="" class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="colItems categoryCards">
                        <div class="categoryCards-content">
                            <h5>Community</h5>
                            <p>There are many new variations of pasages of available text.</p><a href=""
                                class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="colItems categoryCards">
                        <div class="categoryCards-content">
                            <h5>Sustanability</h5>
                            <p>There are many new variations of pasages of available text.</p><a href=""
                                class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="colItems categoryCards">
                        <div class="categoryCards-content">
                            <h5>Integrity</h5>
                            <p>There are many new variations of pasages of available text.</p><a href=""
                                class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section servicesCard-grid">
            <div class="container">
                <h2>Services Card</h2>
                <div class="threeColWithCenteredOrphans" style="--gap-between: 2.4rem">
                    <div class="servicesCard colItems">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-1.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Warm up</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <a href="" class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="servicesCard colItems">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-2.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Spin cycling</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <a href="" class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                    <div class="servicesCard colItems">
                        <div class="servicesCard-image">
                            <img src="assets/img/ser-3.png" alt="" />
                        </div>
                        <div class="servicesCard-content">
                            <h5>Warm up</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <a href="" class="primaryLinkButton">Know More
                                <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="PropertyDetails">
            <div class="container">
                <h2>Services Single</h2>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="SliderBlock">
                            <div class="main">
                                <div class="slider slider-for">
                                    <div><img src="assets/img/components/dummy.png" alt=""></div>
                                    <div><img src="assets/img/components/dummy.png" alt=""></div>
                                    <div><img src="assets/img/components/dummy.png" alt=""></div>
                                    <div><img src="assets/img/components/dummy.png" alt=""></div>
                                    <div><img src="assets/img/components/dummy.png" alt=""></div>
                                </div>
                                <div class="slider slider-nav">
                                    <div><img src="assets/img/components/thumbnail.png" alt=""></div>
                                    <div><img src="assets/img/components/thumbnail.png" alt=""></div>
                                    <div><img src="assets/img/components/thumbnail.png" alt=""></div>
                                    <div><img src="assets/img/components/thumbnail.png" alt=""></div>
                                    <div><img src="assets/img/components/thumbnail.png" alt=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="RightContent">
                            <h2>Architecture</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu est in est
                                accumsan malesuada at sed ipsum. Maecenas eget quam aliquam, finibus lacus eget, iaculis
                                turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulvinar semper
                                massa a varius. Nulla pulvinar lacus non mi ullamcorper laoreet. Aliquam tellus augue,
                                consectetur at luctus sit amet, viverra mollis leo.<br><br> Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit. Pellentesque eu est in est
                                accumsan malesuada at sed ipsum. Maecenas eget quam aliquam, finibus lacus eget, iaculis
                                turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulvinar semper
                                massa a varius. Nulla pulvinar lacus non mi ullamcorper laoreet. Aliquam tellus augue,
                                consectetur at luctus sit amet, viverra mollis leo.</p>
                            <a href="javascript:;" class="primaryCta mt50">
                                <span class="btnText">Contact Us</span>
                                <span class="btnArrow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                        viewBox="0 0 27.552 12.665">
                                        <path
                                            d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                            transform="translate(9.841 -14.624)"></path>
                                    </svg>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section LiteBlueSection OurGallery">
            <div class="container">
                <div class="TopHeading">
                    <h2 class="PinkElement">Gallery Section</h2>
                </div>
                <div class="GalleryBlock">
                    <div class="row">
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/components/news1.png">
                                <img src="assets/img/components/news1.png" />
                            </a>
                        </div>
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/components/news2.png">
                                <img src="assets/img/components/news2.png" />
                            </a>
                        </div>
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/components/news3.png">
                                <img src="assets/img/components/news3.png" />
                            </a>
                        </div>
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/components/news4.png">
                                <img src="assets/img/components/news4.png" />
                            </a>
                        </div>
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/components/news2.png">
                                <img src="assets/img/components/news2.png" />
                            </a>
                        </div>
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/components/news3.png">
                                <img src="assets/img/components/news3.png" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section TestimonialsSec OffWhiteSection">
            <div class="container">
                <h2>Client feedback</h2>
                <div class="testimonialslider">
                    <div class="SlideBlock">
                        <div class="author"><img src="assets/img/person.png" alt=""></div>
                        <h3>This is due to their excellent service, Competitive pricing and customer support. It’s
                            throughly
                            refreshing to get such a personal touch.</h3>
                        <p>Uday Handa / <span>Founder</span></p>
                    </div>
                    <div class="SlideBlock">
                        <div class="author"><img src="assets/img/person.png" alt=""></div>
                        <h3>This is due to their excellent service, Competitive pricing and customer support. It’s
                            throughly
                            refreshing to get such a personal touch.</h3>
                        <p>Uday Handa / <span>Founder</span></p>
                    </div>
                    <div class="SlideBlock">
                        <div class="author"><img src="assets/img/person.png" alt=""></div>
                        <h3>This is due to their excellent service, Competitive pricing and customer support. It’s
                            throughly
                            refreshing to get such a personal touch.</h3>
                        <p>Uday Handa / <span>Founder</span></p>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section faq-sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="TitleHeading">Frequently Asked Questions</h2>
                    </div>
                </div>
                <div class="accordion">

                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="Section VideoBlockSection">
            <div class="container">
                <h2 style="color:#333">Intro Video section</h2>
                <div class="VideoBlock">
                    <img src="assets/img/video.png" alt="">
                    <a href="https://www.youtube.com/watch?v=bY-mOdgz7zQ" class="PlayBtn"><img
                            src="assets/img/play-white.svg" alt=""></a>
                </div>
            </div>
        </section>


        <?php @include('template-parts/ClientSlider.php') ?>



        <section class="Section blogListing">
            <div class="container">
                <div class="blogLisiting-wrapper">
                    <div class="blogListing-title">
                        <h6 class="tagline-text"><span class="small-line"></span> Blogs</h6>
                        <h2>Latest Blogs</h2>
                        <p>We are very proud of the service we provide.</p>
                        <div class="blogSlider-arrowContainer">
                            <button class="blogPrev-btn">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                        viewBox="0 0 27.552 12.665">
                                        <path
                                            d="M-3.979,14.853a.638.638,0,0,1,.636-.212.638.638,0,0,1,.474.474.638.638,0,0,1-.212.636l-4.584,4.59H17.1a.64.64,0,0,1,0,1.278H-7.665L-3.073,26.2a.651.651,0,0,1,0,.9.634.634,0,0,1-.449.187.634.634,0,0,1-.449-.187L-9.65,21.423a.624.624,0,0,1-.191-.449.624.624,0,0,1,.191-.449Z"
                                            transform="translate(9.841 -14.624)" />
                                    </svg>
                                </span>
                            </button>
                            <a href="" class="primaryCta">
                                <span class="btnText"> See all</span>
                                <span class="btnArrow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                        viewBox="0 0 27.552 12.665">
                                        <path
                                            d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                            transform="translate(9.841 -14.624)" />
                                    </svg>
                                </span>
                            </a>
                            <button class="blogNext-btn">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                        viewBox="0 0 27.552 12.665">
                                        <path
                                            d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                            transform="translate(9.841 -14.624)" />
                                    </svg>
                                </span>
                            </button>
                        </div>
                        <a href="" class="primaryCta">
                            <span class="btnText"> See all</span>
                            <span class="btnArrow">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                    viewBox="0 0 27.552 12.665">
                                    <path
                                        d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                        transform="translate(9.841 -14.624)" />
                                </svg>
                            </span>
                        </a>
                    </div>
                    <div class="blogListing-sliderWrapper">
                        <div class="blogListing-slider">
                            <div class="blogListing-slider--slide blogCards">
                                <div class="blog-image">
                                    <img src="assets/img/ser-1.png" alt="" />
                                </div>
                                <h6 class="tagline-text small">
                                    <span class="small-line"></span>March 8, 2020
                                </h6>
                                <h5>What makes engineering consulting different</h5>
                                <a href="" class="primaryLinkButton">
                                    Know More
                                    <?php @include('template-parts/svg-components/link-arrow.php') ?>
                                </a>
                                <p>By Admin / 0 Comments</p>
                            </div>
                            <div class="blogListing-slider--slide blogCards">
                                <div class="blog-image">
                                    <img src="assets/img/ser-2.png" alt="" />
                                </div>
                                <h6 class="tagline-text small">
                                    <span class="small-line"></span>March 8, 2020
                                </h6>
                                <h5>What makes engineering consulting different</h5>
                                <a href="" class="primaryLinkButton">
                                    Know More
                                    <?php @include('template-parts/svg-components/link-arrow.php') ?>
                                </a>
                                <p>By Admin / 0 Comments</p>
                            </div>
                            <div class="blogListing-slider--slide blogCards">
                                <div class="blog-image">
                                    <img src="assets/img/ser-3.png" alt="" />
                                </div>
                                <h6 class="tagline-text small">
                                    <span class="small-line"></span>March 8, 2020
                                </h6>
                                <h5>What makes engineering consulting different</h5>
                                <a href="" class="primaryLinkButton">
                                    Know More
                                    <?php @include('template-parts/svg-components/link-arrow.php') ?>
                                </a>
                                <p>By Admin / 0 Comments</p>
                            </div>
                            <div class="blogListing-slider--slide blogCards">
                                <div class="blog-image">
                                    <img src="assets/img/ser-1.png" alt="" />
                                </div>
                                <h6 class="tagline-text small">
                                    <span class="small-line"></span>March 8, 2020
                                </h6>
                                <h5>What makes engineering consulting different</h5>
                                <a href="" class="primaryLinkButton">
                                    Know More
                                    <?php @include('template-parts/svg-components/link-arrow.php') ?>
                                </a>
                                <p>By Admin / 0 Comments</p>
                            </div>
                            <div class="blogListing-slider--slide blogCards">
                                <div class="blog-image">
                                    <img src="assets/img/ser-2.png" alt="" />
                                </div>
                                <h6 class="tagline-text small">
                                    <span class="small-line"></span>March 8, 2020
                                </h6>
                                <h5>What makes engineering consulting different</h5>
                                <a href="" class="primaryLinkButton">
                                    Know More
                                    <?php @include('template-parts/svg-components/link-arrow.php') ?>
                                </a>
                                <p>By Admin / 0 Comments</p>
                            </div>
                            <div class="blogListing-slider--slide blogCards">
                                <div class="blog-image">
                                    <img src="assets/img/ser-3.png" alt="" />
                                </div>
                                <h6 class="tagline-text small">
                                    <span class="small-line"></span>March 8, 2020
                                </h6>
                                <h5>What makes engineering consulting different</h5>
                                <a href="" class="primaryLinkButton">
                                    Know More
                                    <?php @include('template-parts/svg-components/link-arrow.php') ?>
                                </a>
                                <p>By Admin / 0 Comments</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section singleBlogSection">
            <div class="container">
                <h2>Single Blog</h2>
                <div class="singleBlog-mainImage">
                    <img src="assets/img/ser-2.png" alt="" />
                </div>
                <div class="singleBlogSection-container">
                    <!-- <div class="singleBlog-footNotes">
                        <h6 class="tagline-text">
                            <span class="small-line"></span> March 8, 2021 / By Admin / 0 Comments
                        </h6>
                    </div> -->
                    <div class="singleBlog-body">
                        <h2>What makes engineering consulting different</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                            eu est in est accumsan malesuada at sed ipsum. Maecenas eget quam
                            aliquam, finibus lacus eget, iaculis turpis. Lorem ipsum dolor sit
                            amet, consectetur adipiscing elit. Ut pulvinar semper massa a varius.
                            Nulla pulvinar lacus non mi ullamcorper laoreet. Aliquam tellus augue,
                            consectetur at luctus sit amet, viverra mollis leo. Fusce sed velit
                            eget purus volutpat aliquet nec nec lacus. Orci varius natoque
                            penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        </p>
                        <p>
                            Pellentesque quam libero, consequat non dolor eu, egestas venenatis
                            nunc. Praesent sit amet lorem ultricies, pulvinar sapien sodales,
                            tempor dui. Nam posuere tortor nec nisl rhoncus mattis. Mauris
                            sagittis magna quis neque feugiat dignissim. Integer id ornare turpis.
                            Vestibulum neque purus, tempus placerat pretium ut, ullamcorper at
                            purus.
                        </p>

                        <ul>
                            <li>Item 1</li>
                            <li>Item 4</li>
                            <li>Item 3</li>
                            <li>Item 2</li>
                            <li>Item 5</li>
                        </ul>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                            eu est in est accumsan malesuada at sed ipsum. Maecenas eget quam
                            aliquam, finibus lacus eget, iaculis turpis. Lorem ipsum dolor sit
                            amet, consectetur adipiscing elit. Ut pulvinar semper massa a varius.
                            Nulla pulvinar lacus non mi ullamcorper laoreet. Aliquam tellus augue,
                            consectetur at luctus sit amet, viverra mollis leo. Fusce sed velit
                            eget purus volutpat aliquet nec nec lacus. Orci varius natoque
                            penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Pellentesque quam libero, consequat non dolor eu, egestas venenatis
                            nunc. Praesent sit amet lorem ultricies, pulvinar sapien sodales,
                            tempor dui. Nam posuere tortor nec nisl rhoncus mattis. Mauris
                            sagittis magna quis neque feugiat dignissim. Integer id ornare turpis.
                            Vestibulum neque purus, tempus placerat pretium ut, ullamcorper at
                            purus.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="sec-16 TabSec">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wrapper">
                        <div class="tab_btn--wrapper">
                            <button data-toggle="tab" data-tabs=".gtabs" data-tab=".tab-1"
                                class="btn-tab active">Architecture</button>
                            <button data-toggle="tab" data-tabs=".gtabs" data-tab=".tab-2" class="btn-tab">Concrete
                                Transport</button>
                            <button data-toggle="tab" data-tabs=".gtabs" data-tab=".tab-3" class="btn-tab">Tabe
                                Name</button>
                            <button data-toggle="tab" data-tabs=".gtabs" data-tab=".tab-4" class="btn-tab">Tabe
                                Name</button>
                            <button data-toggle="tab" data-tabs=".gtabs" data-tab=".tab-5" class="btn-tab">Tabe
                                Name</button>
                        </div>
                        <div class="gtabs">
                            <div class="gtab active tab-1">
                                <h2 class="sub_heading TitleHeading">TabName</h2>
                                <p class="subcopy">
                                    Phasellus malesuada sapien vitae scelerisque facilisis. Curabitur elementum eget
                                    risus nec rhoncus. Maecenas et consequat nibh, in porttitor ex. Ut congue purus ac
                                    ornare vehicula. Phasellus eu sodales dolor. Praesent commodo sem eget accumsan
                                    vestibulum. Praesent fringilla at lorem vitae viverra.
                                </p>
                                <div class="lists">
                                    <p class="list">
                                        <span class="subcopy">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit.</span>
                                    </p>
                                    <p class="list">
                                        <span class="subcopy">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit.</span>
                                    </p>
                                    <p class="list">
                                        <span class="subcopy">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit.</span>
                                    </p>
                                    <p class="list">
                                        <span class="subcopy">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit.</span>
                                    </p>
                                    <p class="list">
                                        <span class="subcopy">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit.</span>
                                    </p>
                                </div>
                            </div>
                            <div class="gtab tab-2">
                                <h2 class="sub_heading TitleHeading">TabName</h2>
                                <p class="subcopy">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipiscelit. Nisi minima fugit, est facere molestiae quod pariatur.
                                    Consectetur natus, blanditiis laborum possimus doloremque harum adipisci debitis
                                    similique, nostrum provident ut dolelit. Nisi minima fugit, est facere molestiae
                                    quod pariatur. Consectetur natus, blanditiis laborum possimus doloremque harum
                                    adipisci debitis similique, nostrum provident ut dolelit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipisci debitis similique, nostrum provident ut doli debitis
                                    similique, nostrum provident ut dolore.
                                </p>
                            </div>
                            <div class="gtab tab-3">
                                <h2 class="sub_heading TitleHeading">TabName</h2>
                                <p class="subcopy">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipiscelit. Nisi minima fugit, est facere molestiae quod pariatur.
                                    Consectetur natus, blanditiis laborum possimus doloremque harum adipisci debitis
                                    similique, nostrum provident ut dolelit. Nisi minima fugit, est facere molestiae
                                    quod pariatur. Consectetur natus, blanditiis laborum possimus doloremque harum
                                    adipisci debitis similique, nostrum provident ut dolelit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipisci debitis similique, nostrum provident ut doli debitis
                                    similique, nostrum provident ut dolore.
                                </p>
                            </div>

                            <div class="gtab tab-4">
                                <h2 class="sub_heading TitleHeading">TabName</h2>
                                <p class="subcopy">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipiscelit. Nisi minima fugit, est facere molestiae quod pariatur.
                                    Consectetur natus, blanditiis laborum possimus doloremque harum adipisci debitis
                                    similique, nostrum provident ut dolelit. Nisi minima fugit, est facere molestiae
                                    quod pariatur. Consectetur natus, blanditiis laborum possimus doloremque harum
                                    adipisci debitis similique, nostrum provident ut dolelit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipisci debitis similique, nostrum provident ut doli debitis
                                    similique, nostrum provident ut dolore.
                                </p>
                            </div>

                            <div class="gtab tab-5">
                                <h2 class="sub_heading TitleHeading">TabName</h2>
                                <p class="subcopy">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipiscelit. Nisi minima fugit, est facere molestiae quod pariatur.
                                    Consectetur natus, blanditiis laborum possimus doloremque harum adipisci debitis
                                    similique, nostrum provident ut dolelit. Nisi minima fugit, est facere molestiae
                                    quod pariatur. Consectetur natus, blanditiis laborum possimus doloremque harum
                                    adipisci debitis similique, nostrum provident ut dolelit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipisci debitis similique, nostrum provident ut doli debitis
                                    similique, nostrum provident ut dolore.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section bookingFrom FormDropdownelement">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-7">
                        <h2>Booking Form</h2>
                        <div class="BookingFormWrapper">
                            <h6 class="tagline-text"><span class="small-line"></span>Contact Us</h6>
                            <h2>Fill the form to contact us.</h2>
                            <button class="closePopupBtn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15.563" height="15.563"
                                    viewBox="0 0 15.563 15.563">
                                    <path class="a"
                                        d="M41.2,55.663l6.681-6.681,6.681,6.681,1.1-1.1-6.681-6.681L55.663,41.2l-1.1-1.1-6.681,6.681L41.2,40.1l-1.1,1.1,6.681,6.681L40.1,54.563Z"
                                        transform="translate(-40.1 -40.1)" />
                                </svg>
                            </button>
                            <form action="" class="bookingForm-form">
                                <div class="leftRightContainer formFields-row" style="--gap-between: 1.2rem">
                                    <input class="formFieldInput" type="text" placeholder="Full Name" />
                                    <input class="formFieldInput" type="text" placeholder="Email" />
                                </div>
                                <div class="leftRightContainer formFields-row" style="--gap-between: 1.2rem">
                                    <input class="formFieldInput" type="text" placeholder="Full Name" />
                                    <input class="formFieldInput" type="text" placeholder="Email" />
                                </div>
                                <div class="formFields-row">
                                    <textarea class="formFieldTextArea" placeholder="Message"></textarea>
                                </div>
                                <div class="formFields-row">
                                    <button class="primaryCta">
                                        <span class="btnText">Submit</span>
                                        <span class="btnArrow">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                                viewBox="0 0 27.552 12.665">
                                                <path
                                                    d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                                    transform="translate(9.841 -14.624)" />
                                            </svg>
                                        </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-md-5">
                        <h2 class="TitleHeading">Dropdown menu</h2>
                        <ul class="dropdown">
                            <li>
                                <a href="#">Buy a Home</a>
                            </li>
                            <li>
                                <a href="#">Sell a Home</a>
                            </li>
                            <li>
                                <a href="#">Rent a Home</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section contactUsSection">
            <div class="container">
                <h2>Contact Us</h2>
                <div class="contactUsWrapper">
                    <div class="contactUs-address">
                        <div class="contactUs-address--icon">
                            <?php @include('template-parts/svg-components/location-pin.php') ?>
                        </div>
                        <div class="contactUs-address--content">
                            <h5>South Delhi</h5>
                            <address>
                                Thinkvalley B13, Sector32, Gurgaon - 122002, Haryana (India)
                            </address>
                        </div>
                    </div>
                    <div class="contactUs-email">
                        <div class="contactUs-address--icon">
                            <?php @include('template-parts/svg-components/email.php') ?>
                        </div>
                        <div class="contactUs-address--content">
                            <h5>Email</h5>
                            <address>
                                hi@cobold.in
                            </address>
                        </div>
                    </div>
                    <div class="contactUs-phone">
                        <div class="contactUs-address--icon">
                            <?php @include('template-parts/svg-components/phone.php') ?>
                        </div>
                        <div class="contactUs-address--content">
                            <h5>Phone</h5>
                            <address>
                                +91 - 981079XXXX
                            </address>
                        </div>
                    </div>
                    <div class="contactUs-contact--form">
                        <div class="BookingFormWrapper">
                            <h2>Fill the form to contact us.</h2>
                            <p>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Debitis,
                                natus
                            </p>
                            <form action="" class="contactForm-form">
                                <div class="leftRightContainer formFields-row" style="--gap-between: 1.2rem">
                                    <input class="formFieldInput" type="text" placeholder="Full Name" />
                                    <input class="formFieldInput" type="text" placeholder="Email" />
                                </div>
                                <div class="leftRightContainer formFields-row" style="--gap-between: 1.2rem">
                                    <input class="formFieldInput" type="text" placeholder="Full Name" />
                                    <input class="formFieldInput" type="text" placeholder="Email" />
                                </div>
                                <div class="formFields-row">
                                    <textarea class="formFieldTextArea" placeholder="Message"></textarea>
                                </div>
                                <div class="formFields-row">
                                    <button class="primaryCta">
                                        <span class="btnText">Submit</span>
                                        <span class="btnArrow">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="27.552" height="12.665"
                                                viewBox="0 0 27.552 12.665">
                                                <path
                                                    d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                                                    transform="translate(9.841 -14.624)" />
                                            </svg>
                                        </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section>
            <iframe style="display: block;"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3506.6254739568285!2d77.14354531446284!3d28.490823697232504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1e23b39383f5%3A0xf03adb9a3b5fa5a!2sCobold%20Digital!5e0!3m2!1sen!2sin!4v1618915905557!5m2!1sen!2sin"
                width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </section>
        <?php @include('template-parts/footer.php') ?>