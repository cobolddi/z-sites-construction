<?php @include('template-parts/header.php') ?>
<main>
  <section class="Section extraPadded hp-heroSection">
    <div class="container">
      <div class="heroWrapper">
        <div class="heroContent">
          <h6 class="tagline-text">
            <span class="small-line"></span>construction
          </h6>
          <h1>JCB HEAVY CONSTRUCTION EQUIPMENT</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
            quam, vulputate in laoreet quis, tempus non diam.
          </p>
          <a href="" class="primaryCta">
            <span class="btnText">Contact Us</span>
            <span class="btnArrow">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="27.552"
                height="12.665"
                viewBox="0 0 27.552 12.665"
              >
                <path
                  d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                  transform="translate(9.841 -14.624)"
                />
              </svg>
            </span>
          </a>
        </div>
        <div class="heroImage">
          <span class="circleDec"></span>
          <img src="assets/img/jcb-home.png" alt="" />
        </div>
      </div>
    </div>
  </section>

  <section class="Section extraPadded rightImageLeftContentSection hp-history">
    <div class="container">
      <div class="leftRightContainer" style="--gap-between: 8rem">
        <div class="left">
          <img src="assets/img/jcb-home 2.png" alt="" />
        </div>
        <div class="right">
          <h6 class="tagline-text">
            <span class="small-line"></span> History
          </h6>
          <h2>Focused and Future Ready</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue
            quam, vulputate in laoreet quis, tempus non diam. Donec placerat
            quam vel purus semper ultricies. Pellentesque dictum sed libero non
            feugiat. Ut in lacus sit amet risus convallis sollicitudin. Nam
            pulvinar elementum commodo.
          </p>
          <p>
            Ut tortor sapien, tempus in tincidunt sit amet, pulvinar non justo.
            Ut in lacus sit amet risus convallis sollicitudin. Nam pulvinar
            elementum commodo. Ut in lacus sit amet risus convallis
            sollicitudin. Nam pulvinar elementum commodo.
          </p>
          <a href="" class="primaryCta">
            <span class="btnText">Read More</span>
            <span class="btnArrow">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="27.552"
                height="12.665"
                viewBox="0 0 27.552 12.665"
              >
                <path
                  d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                  transform="translate(9.841 -14.624)"
                />
              </svg>
            </span>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="Section extraPadded ourCategory hp-category">
    <div class="container">
      <div class="centeredHeadings">
        <h6 class="tagline-text">
          <span class="small-line"></span>Our Values
        </h6>
        <h2>CERTIFICATIONS</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
          tincidunt massa id leo tincidunt, vehicula fringilla ante finibus.
        </p>
      </div>

      <div class="fourColWithCenteredOrphans">
        <div class="colItems categoryCards">
          <div class="categoryCards-icon">
            <?php @include('template-parts/svg-components/safety.php') ?>
          </div>
          <div class="categoryCards-content">
            <h5>Safety</h5>
            <p>There are many new variations of pasages of available text.</p>
          </div>
        </div>
        <div class="colItems categoryCards">
          <div class="categoryCards-icon">
            <?php @include('template-parts/svg-components/community.php') ?>
          </div>
          <div class="categoryCards-content">
            <h5>Community</h5>
            <p>There are many new variations of pasages of available text.</p>
          </div>
        </div>
        <div class="colItems categoryCards">
          <div class="categoryCards-icon">
            <?php @include('template-parts/svg-components/sustain.php') ?>
          </div>
          <div class="categoryCards-content">
            <h5>Sustanability</h5>
            <p>There are many new variations of pasages of available text.</p>
          </div>
        </div>
        <div class="colItems categoryCards">
          <div class="categoryCards-icon">
            <?php @include('template-parts/svg-components/integrity.php') ?>
          </div>
          <div class="categoryCards-content">
            <h5>Integrity</h5>
            <p>There are many new variations of pasages of available text.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Section extraPadded hp-newArrivals">
    <div class="container">
      <div class="hp-newArrivals--heading">
        <div class="titles">
          <h6 class="tagline-text">
            <span class="small-line"></span>Services
          </h6>
          <h2>WHAT WE DO</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
            tincidunt massa id leo tincidunt, vehicula fringilla ante finibus.
          </p>
        </div>
        <a href="" class="primaryCta">
          <span class="btnText">See All</span>
          <span class="btnArrow">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="27.552"
              height="12.665"
              viewBox="0 0 27.552 12.665"
            >
              <path
                d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                transform="translate(9.841 -14.624)"
              />
            </svg>
          </span>
        </a>
      </div>
      <div class="hp-newArrivals--list">
        <div class="threeColWithCenteredOrphans" style="--gap-between: 2.4rem">
          <div class="servicesCard colItems">
            <div class="servicesCard-image">
              <img src="assets/img/ser-1.png" alt="" />
            </div>
            <div class="servicesCard-content">
              <h5>CONCRETE TRANSPORT</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              <a href="" class="primaryLinkButton"
                >Know More
                <?php @include('template-parts/svg-components/link-arrow.php') ?></a
              >
            </div>
          </div>
          <div class="servicesCard colItems">
            <div class="servicesCard-image">
              <img src="assets/img/ser-2.png" alt="" />
            </div>
            <div class="servicesCard-content">
              <h5>ARCHITECTURE</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              <a href="" class="primaryLinkButton"
                >Know More
                <?php @include('template-parts/svg-components/link-arrow.php') ?></a
              >
            </div>
          </div>
          <div class="servicesCard colItems">
            <div class="servicesCard-image">
              <img src="assets/img/ser-3.png" alt="" />
            </div>
            <div class="servicesCard-content">
              <h5>CONSULTING</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              <a href="" class="primaryLinkButton"
                >Know More
                <?php @include('template-parts/svg-components/link-arrow.php') ?></a
              >
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="Section extraPadded VideoBlockSection neutralBg">
    <div class="container">
      <h2>Videos</h2>
      <div class="VideoBlock">
        <img src="assets/img/video.png" alt="" />
        <a href="https://www.youtube.com/watch?v=bY-mOdgz7zQ" class="PlayBtn"
          ><img src="assets/img/play-white.svg" alt=""
        /></a>
      </div>
    </div>
  </section>

  <section class="Section extraPadded blogListing">
    <div class="container">
      <div class="blogLisiting-wrapper">
        <div class="blogListing-title">
          <h6 class="tagline-text"><span class="small-line"></span> Blogs</h6>
          <h2>Latest Blogs</h2>
          <p>We are very proud of the service we provide.</p>
          <div class="blogSlider-arrowContainer">
            <button class="blogPrev-btn">
              <span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="27.552"
                  height="12.665"
                  viewBox="0 0 27.552 12.665"
                >
                  <path
                    d="M-3.979,14.853a.638.638,0,0,1,.636-.212.638.638,0,0,1,.474.474.638.638,0,0,1-.212.636l-4.584,4.59H17.1a.64.64,0,0,1,0,1.278H-7.665L-3.073,26.2a.651.651,0,0,1,0,.9.634.634,0,0,1-.449.187.634.634,0,0,1-.449-.187L-9.65,21.423a.624.624,0,0,1-.191-.449.624.624,0,0,1,.191-.449Z"
                    transform="translate(9.841 -14.624)"
                  />
                </svg>
              </span>
            </button>
            <a href="" class="secondaryCta">
              <span class="btnText"> See all</span>
              <span class="btnArrow">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="27.552"
                  height="12.665"
                  viewBox="0 0 27.552 12.665"
                >
                  <path
                    d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                    transform="translate(9.841 -14.624)"
                  />
                </svg>
              </span>
            </a>
            <button class="blogNext-btn">
              <span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="27.552"
                  height="12.665"
                  viewBox="0 0 27.552 12.665"
                >
                  <path
                    d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                    transform="translate(9.841 -14.624)"
                  />
                </svg>
              </span>
            </button>
          </div>
          <a href="" class="primaryCta">
            <span class="btnText"> See all</span>
            <span class="btnArrow">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="27.552"
                height="12.665"
                viewBox="0 0 27.552 12.665"
              >
                <path
                  d="M11.847,14.853a.638.638,0,1,0-.9.9l4.584,4.59H-9.236a.64.64,0,0,0-.606.639.64.64,0,0,0,.606.639H15.533L10.942,26.2a.651.651,0,0,0,0,.9.634.634,0,0,0,.9,0l5.679-5.679a.624.624,0,0,0,0-.9Z"
                  transform="translate(9.841 -14.624)"
                />
              </svg>
            </span>
          </a>
        </div>
        <div class="blogListing-sliderWrapper">
          <div class="blogListing-slider">
            <div class="blogListing-slider--slide blogCards">
              <div class="blog-image">
                <img src="assets/img/blog-1.png" alt="" />
              </div>
              <h6 class="tagline-text small">
                <span class="small-line"></span>March 8, 2020
              </h6>
              <h5>How to choose the Perfect Planner</h5>
              <a href="" class="primaryLinkButton">
                Know More
                <?php @include('template-parts/svg-components/link-arrow.php') ?>
              </a>
              <p>By Admin / 0 Comments</p>
            </div>
            <div class="blogListing-slider--slide blogCards">
              <div class="blog-image">
                <img src="assets/img/blog-2.png" alt="" />
              </div>
              <h6 class="tagline-text small">
                <span class="small-line"></span>March 8, 2020
              </h6>
              <h5>How to choose the Perfect Planner</h5>
              <a href="" class="primaryLinkButton">
                Know More
                <?php @include('template-parts/svg-components/link-arrow.php') ?>
              </a>
              <p>By Admin / 0 Comments</p>
            </div>
            <div class="blogListing-slider--slide blogCards">
              <div class="blog-image">
                <img src="assets/img/blog-3.png" alt="" />
              </div>
              <h6 class="tagline-text small">
                <span class="small-line"></span>March 8, 2020
              </h6>
              <h5>How to choose the Perfect Planner</h5>
              <a href="" class="primaryLinkButton">
                Know More
                <?php @include('template-parts/svg-components/link-arrow.php') ?>
              </a>
              <p>By Admin / 0 Comments</p>
            </div>
            <div class="blogListing-slider--slide blogCards">
              <div class="blog-image">
                <img src="assets/img/c2.png" alt="" />
              </div>
              <h6 class="tagline-text small">
                <span class="small-line"></span>March 8, 2020
              </h6>
              <h5>Solid Wood Arizona Study Table</h5>
              <a href="" class="primaryLinkButton">
                Know More
                <?php @include('template-parts/svg-components/link-arrow.php') ?>
              </a>
              <p>By Admin / 0 Comments</p>
            </div>
            <div class="blogListing-slider--slide blogCards">
              <div class="blog-image">
                <img src="assets/img/c2.png" alt="" />
              </div>
              <h6 class="tagline-text small">
                <span class="small-line"></span>March 8, 2020
              </h6>
              <h5>Solid Wood Arizona Study Table</h5>
              <a href="" class="primaryLinkButton">
                Know More
                <?php @include('template-parts/svg-components/link-arrow.php') ?>
              </a>
              <p>By Admin / 0 Comments</p>
            </div>
            <div class="blogListing-slider--slide blogCards">
              <div class="blog-image">
                <img src="assets/img/c2.png" alt="" />
              </div>
              <h6 class="tagline-text small">
                <span class="small-line"></span>March 8, 2020
              </h6>
              <h5>Solid Wood Arizona Study Table</h5>
              <a href="" class="primaryLinkButton">
                Know More
                <?php @include('template-parts/svg-components/link-arrow.php') ?>
              </a>
              <p>By Admin / 0 Comments</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Section extraPadded TestimonialsSec neutralBg">
    <div class="container">
      <h2>Client feedback</h2>
      <div class="testimonialslider">
        <div class="SlideBlock">
          <div class="author">
            <img src="assets/img/person.png" alt="" />
          </div>
          <h3>
            This is due to their excellent service, Competitive pricing and
            customer support. It’s throughly refreshing to get such a personal
            touch.
          </h3>
          <p>Uday Handa / <span>Founder</span></p>
        </div>
        <div class="SlideBlock">
          <div class="author">
            <img src="assets/img/test-person.png" alt="" />
          </div>
          <h3>
            This is due to their excellent service, Competitive pricing and
            customer support. It’s throughly refreshing to get such a personal
            touch.
          </h3>
          <p>Uday Handa / <span>Founder</span></p>
        </div>
        <div class="SlideBlock">
          <div class="author">
            <img src="assets/img/test-person.png" alt="" />
          </div>
          <h3>
            This is due to their excellent service, Competitive pricing and
            customer support. It’s throughly refreshing to get such a personal
            touch.
          </h3>
          <p>Uday Handa / <span>Founder</span></p>
        </div>
      </div>
    </div>
  </section>
</main>

<?php @include('template-parts/footer.php') ?>
