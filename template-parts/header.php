<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Concept Construction</title>
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
</head>

<body>
    <main>
        <header class="main-header">
            <div class="logo">
                <a href="index.php"><img src="assets/img/logo-black.svg" alt="" /></a>
            </div>
            <div class="navLink-container">
                <input type="checkbox" class="menu-btn" id="menu-btn" />
                <label for="menu-btn" class="menu-icon">
                    <span class="menu-icon__line"></span>
                </label>

                <ul class="nav-links">
                    <li class="nav-link">
                        <a href="/">
                            Home
                        </a>

                    </li>
                    <li class="nav-link">
                        <a href="">About Us</a>
                    </li>
                    <li class="nav-link">
                        <a href="">Services</a>
                    </li>
                    <li class="nav-link">
                        <a href="javascript:void(0)">
                            Blog
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="">Contact Us</a>
                    </li>

                </ul>


            </div>
        </header>