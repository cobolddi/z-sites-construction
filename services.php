<?php @include('template-parts/header.php') ?>
<main>
    <section class="Section about">
        <div class="container">
            <h2>Services</h2>
            <p>Home / Services</p>
        </div>
    </section>

    <section class="Section extraPadded sp-servicesLisitng">
        <div class="container">
            <div class="centeredHeadings">
                <h6 class="tagline-text"><span class="small-line"></span>Services</h6>
                <h2>Your body is a work of art in the gym</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
                    tincidunt massa id leo tincidunt, vehicula fringilla ante finibus.
                </p>
            </div>
            <div class="threeColWithCenteredOrphans" style="--gap-between: 2.4rem">
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
                <div class="servicesCard colItems">
                    <div class="servicesCard-image">
                        <img src="assets/img/ser-3.png" alt="" />
                    </div>
                    <div class="servicesCard-content">
                        <h5>Warm up</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="" class="primaryLinkButton">Know More
                            <?php @include('template-parts/svg-components/link-arrow.php') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php @include('template-parts/footer.php') ?>